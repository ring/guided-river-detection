import numpy as np
from utils import *
import matplotlib.pyplot as plt
from RiversSegmentationFromImage import SegementationMultipleRivers

sar_image =  plt.imread('./Data/Redon_vh_20180704.tif').astype(np.float)

coords_a_priori = np.array([[1, 617, 276,2, 148],[2, 374, 256,227, 515],[3,244,455, 65,771]])
reach_to_river = np.array([[1, 1],[2, 2],[3, 2]])

scalemin = 1
scalemax = 4
beta = 15
n_pow = 10
eta = 6
sar_image_intensity = sar_image ** 2
swot = False

plt.figure()
plt.imshow(sar_image, vmin=0, vmax=display_threshold(sar_image), cmap='gray')
plt.title("SAR Image " )
plt.show(block = False)

lines_detector_response, centerline, segmentation, centerlines  = SegementationMultipleRivers(sar_image_intensity, coords_a_priori,reach_to_river = reach_to_river,beta =beta , h = 0.2, scalemax = scalemax, swot = swot, pow_line_detector = n_pow, eta = eta)

plt.figure()
plt.imshow(return_centerline_on_LDR(lines_detector_response, np.zeros_like(lines_detector_response)), aspect='equal', interpolation = None)
plt.title("Linear structures detector response" )
plt.show(block = False)


plt.figure()
plt.imshow(color_display(sar_image,centerline), aspect='equal', interpolation = None)
plt.title("Centerlines of the rivers" )
plt.show(block = False)

plt.figure()
plt.imshow(color_display(sar_image,segmentation,2), aspect='equal', interpolation = None)
plt.title('Water Detection')
plt.show(block = False)


plt.show()


