# guided-river-detection

Python implementation of the guided narrow river detection approach for SAR images proposed by N.Gasnier, L.Denis, F.Liège, R.Fjørtoft, and F.Tupin in [Narrow River Extraction from SAR ImagesUsing Exogenous Information (IEEE Journal of Selected Topics in Applied Earth Observations and Remote Sensing](https://ieeexplore.ieee.org/document/9440665)).


**Usage**
The main function of the module is `SegementationMultipleRivers()` and can be loaded as follows : 

`from RiversSegmentationFromImage import SegementationMultipleRivers`

A simple example of usage can be found in the `example.py` script. It runs on the `./Data/Redon_vh_20180704.tif`image, but with an additional pair of nodes compared to its counterpart in the article.

Its a priori river nodes are defined as follows:

`coords_a_priori = np.array([[1, 617, 276,2, 148],[2, 374, 256,227, 515],[3,244,455, 65,771]])`

Each element in the list is a pair of a priori nodes. For example, `[1, 617, 276,2, 148]` means the pair of nodes number **1** has two nodes whose **(y,x)** coordinates are **(617,276)** and **(2,148)**. Note that the number associated with pair of nodes have to be consecutive integers.

The correspondance between each pair of nodes and a river number is given as follows:

`reach_to_river = np.array([[1, 1],[2, 2],[3, 2]])`

Where each element `[n_p, n_r]` in the list means that the pair of nodes `n_p` belongs to the river number `n_r`. Here, we know one pair of nodes for the first river while we have two overlapping pair of nodes for the second river.

The main function is then called as follows, with the parameters set as presented in the article:

`lines_detector_response, centerline, segmentation, centerlines  = SegementationMultipleRivers(sar_image_intensity, coords_a_priori,reach_to_river = reach_to_river,beta =beta , h = 0.2, scalemax = scalemax, swot = swot, pow_line_detector = n_pow, eta = eta)`







**Reproducing the results from the article**

The results for any Sentinel-1 image presented in the [article](https://ieeexplore.ieee.org/document/9440665) can be reproduced by running `python3 main.py` and entering the image number. SWOT simulated images are not available for public release and are not included in this repository.

Note that the results of our experiments have been obtained with specific versions of third-party components. In particular numpy v.1.18.5, scipy v.1.4.1 and scikit-images v.0.15.0. Using newer versions of these libraries, notably scikit-images might result in slight differences in the segmentation.


**Copyright and license**

 Copyright 2020 CS GROUP - France, Centre national d'études spatiales, Télécom Paris.

The guided river detection software as a whole is distributed under the GNU GPL v3.0. license. A copy of this license is available in the LICENSE file. Guided river detection depends on third-party components and code snippets released under their own license (obviously, all compatible with the one of guided river detection). These dependencies are listed in the NOTICE file.

The guided river detection documentation (available in the doc directory) is distributed under the Creative Commons Attribution 4.0 International license (CC BY 4.0). A copy of this license is available in the LICENSE.CC-BY-4.0 file.

All code snippets provided in the documentation, tutorials and examples (available in the doc and examples directories) are intended to show how to use guided river detection and are not, strictly speaking, part of guided river detection project. In order to facilitate their reuse, they are released under the “Zero-Clause BSD” license (aka BSD 0-Clause or 0BSD). This is a permissive license that allows you to copy and paste this source code without preserving copyright notices. A copy of this license is available in the LICENSE.0BSD file.
