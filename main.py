# coding: utf8
"""
.. module:: Narrow Rivers Extraction
    :synopsis: Main file:  loads the images, runs the computation and displays the results.

.. moduleauthor:: Nicolas Gasnier - CS GROUP - CNES - Télécom Paris

..
   This file is part of the river detection demonstrator for N.Gasnier et al. « Narrow River Extraction from SAR Images
   Using Exogenous Information submitted to IEEE JSTARS »
   Copyright (C) 2020 Centre National d’Etudes Spatiales, CS GROUP, Télécom Paris
   This software is released under open source license GPL v.3 and is distributed WITHOUT ANY WARRANTY, read LICENSE.txt for further details.

"""


import numpy as np
from utils import *
import matplotlib.pyplot as plt
matplotlib.use('Agg')   #    Comment to enable figures display
from line_detector import *
from RiversSegmentationFromImage import SegementationMultipleRivers
from skimage.morphology import binary_dilation as dilation

save = True # If true, the figures will be saved in the "Results" folder
path = './Results/'

try:
    image_number = int(input('Choose an image number:  '))
    assert image_number > 0 and image_number<11, 'unknown image number'
except:
    ('Invalid image number')
if image_number < 8:
    swot = False
else:
    swot = True
    print('SWOT images are not available for the public demonstrator')
print("Processing image ", str(image_number))
loadSARImage = {
        1:loadDesMoinesGRD,
        2:loadSagarGRD,
        3:loadGambieGRD,
        4:loadAngersGRD,
        5:loadToulouseGRD,
        6:loadRedonGRD,
        7:loadArataiGRD}

loadTruth = {
        1:loadDesMoinesTruth,
        2:loadSagarTruth,
        3:loadGambieTruth,
        4:loadAngersTruth,
        5:loadToulouseTruth,
        6:loadRedonTruth,
        7:loadArataiTruth}

if swot:
    image_sar, truth, baseline = loadSARImage[image_number]()
    truth = loadTruth[image_number]()
else:
    image_sar = loadSARImage[image_number]()
    truth = loadTruth[image_number]()


# For each river reach, coords_a_priori gives 5 ints: the label of the reach and y_0, x_0, y_1, x_1
#the labels have to be consecutive integers


coords_a_priori = {
        1:np.array([[1, 550, 0,217, 1111]]),
        2:np.array([[1, 1000, 1,220, 915]]),
        3:np.array([[1, 736, 544,22, 988], [2, 736, 544,1304, 1382], [3, 736, 544,1310, 112]]),
        4:np.array([[1, 920, 109,774, 504], [2, 774, 504,665, 1062], [3, 20, 800,660, 210], [4, 915, 25,636, 1848]]),
        5:np.array([[1, 1104, 1341,10, 285]]),
        6:np.array([[1, 617, 276,2, 148],[2, 374, 256, 65,771]]),
        7:np.array([[1, 0, 153,414, 995],[2, 0, 980,550, 971]]),
        8:np.array([[1, 95, 0,100, 200], [2,97,121,147,268],  [3,147,248,270,348]]),
        9:np.array([[1, 698, 60,316, 799]]),
        10:np.array([[1, 0, 111,795, 44], [2,0,256,798,178],  [3,0,691,798,269], [4,657,80,779,182]])}
coords_a_priori = coords_a_priori[image_number]

# Reach_to_river gives the correspondance between reach numbers and river numbers.
reach_to_river = {
        1:np.array([[1, 1]]),
        2:np.array([[1, 1]]),
        3:np.array([[1, 1],[2, 2],[3, 3]]),
        4:np.array([[1, 1],[2, 1],[3, 2],[4, 3]]),
        5:np.array([[1, 1]]),
        6:np.array([[1, 1],[2, 2]]),
        7:np.array([[1,1],[2,2]]),
        8:np.array([[1, 1],[2, 1],[3, 1]]),
        9:np.array([[1, 1]]),
        10:np.array([[1, 1],[2, 2],[3, 3],[4, 4]])}
reach_to_river = reach_to_river[image_number]


if swot is True:
    scalemin = 1
    scalemax = 3
    n_pow = 70
    eta = 6
    beta = 15
    sar_image_intensity = image_sar**2
else:
    scalemin = 1
    scalemax = 4
    beta = 15
    n_pow = 10
    eta = 6
    sar_image_intensity = image_sar**2

plt.figure()
plt.imshow(image_sar, vmin=0, vmax=display_threshold(image_sar), cmap='gray',aspect='equal', interpolation = None)
title = path + 'SarImage_' + str(image_number) +'.eps'
plt.axis('off')
if save:
    plt.savefig(title,format = 'eps',pad_inches=0,bbox_inches='tight',dpi = 300)
else:
    plt.title("SAR Image n°"+ str(image_number) )
plt.show(block = False)


lines_detector_response, centerline, segmentation, centerlines  = SegementationMultipleRivers(sar_image_intensity, coords_a_priori,reach_to_river = reach_to_river,beta =beta , h = 0.2, scalemax = scalemax, swot = swot, pow_line_detector = n_pow, eta = eta)



plt.figure()
plt.imshow(image_sar, vmin=0, vmax=display_threshold(image_sar), cmap='gray',aspect='equal', interpolation = None)
title = path + 'Linear_structure_detector_response_' + str(image_number) +'.eps'
plt.axis('off')
if save:
    plt.savefig(title,format = 'eps',pad_inches=0,bbox_inches='tight',dpi = 300)
else:
    plt.title("Linear structure detector response n°"+ str(image_number) )
plt.show(block = False)

plt.figure()
plt.imshow(return_centerline_on_LDR(lines_detector_response, centerline), aspect='equal', interpolation = None)
title = path + 'Linear_detector_centerline_' + str(image_number) + '.eps'
plt.axis('off')
if save:
    plt.savefig(title,format = 'eps',pad_inches=0,bbox_inches='tight',dpi = 300)
else:
    plt.title("Linear structure detector response and centerline n°" + str(image_number) )
plt.show(block = False)


plt.figure()
plt.imshow(color_display(image_sar,centerlines), aspect='equal', interpolation = None)
title = path + 'Image_and_centerline_' + str(image_number) + '.eps'
plt.axis('off')
if save:
    plt.savefig(title,format = 'eps',pad_inches=0,bbox_inches='tight',dpi = 300)
else:
    plt.title("Image and centerline n°" + str(image_number) )
plt.show(block = False)



plt.figure()
plt.imshow(color_display(image_sar,dilation(segmentation)-segmentation,2), aspect='equal', interpolation = None)
title = path + 'Water_Detection_Contour_' + str(image_number) + '.eps'
plt.axis('off')
if save:
    plt.savefig(title,format = 'eps',pad_inches=0,bbox_inches='tight',dpi = 300)
else:
    plt.title("Water Detection Contour n°" + str(image_number) )
plt.show(block = False)

plt.figure()
plt.imshow(color_display(image_sar,segmentation), aspect='equal', interpolation = None)
title = path + 'Water_Detection_' + str(image_number) + '.eps'
plt.axis('off')
if save:
    plt.savefig(title,format = 'eps',pad_inches=0,bbox_inches='tight',dpi = 300)
else:
    plt.title("Water Detection n°" + str(image_number) )
plt.show(block = False)



plt.figure()
plt.imshow(return_centerline_on_LDR(lines_detector_response, centerline), aspect='equal', interpolation = None)
title = path + 'Linear_detector_centerline_' + str(image_number) + '.eps'
plt.axis('off')
if save:
    plt.savefig(title,format = 'eps',pad_inches=0,bbox_inches='tight',dpi = 300)
else:
    plt.title("Linear structure detector response and centerline n°" + str(image_number) )
plt.show(block = False)

plt.figure()
plt.imshow(color_display_error_uncertain(image_sar,segmentation,truth), aspect='equal', interpolation = None)
title = path + 'Detection_Error_' + str(image_number) + '.eps'
plt.axis('off')
if save:
    plt.savefig(title,format = 'eps',pad_inches=0,bbox_inches='tight',dpi = 300)
else:
    plt.title("Detection Error n°" + str(image_number) )
plt.show(block = False)

print('Metrics for proposed method:')
DisplayDetectionError(segmentation>0, truth)

if swot:
    plt.figure()
    plt.imshow(color_display_error_uncertain(image_sar,baseline,truth), aspect='equal', interpolation = None)
    title = path + 'Baseline detection_Error_' + str(image_number) + '.eps'
    plt.axis('off')
    if save:
        plt.savefig(title,format = 'eps',pad_inches=0,bbox_inches='tight',dpi = 300)
    else:
        plt.title("Baseline detection Error n°" + str(image_number) )
    plt.show(block = False)
    print('Metrics for baseline')
    DisplayDetectionError(baseline>0, truth)

plt.show()

