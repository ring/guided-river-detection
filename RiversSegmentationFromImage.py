# coding: utf8
"""
.. module:: Narrow Rivers Extraction
    :synopsis: Main file:  loads the images, runs the computation and displays the results.

.. moduleauthor:: Nicolas Gasnier - CS GROUP - CNES - Télécom Paris

..
   This file is part of the river detection demonstrator for N.Gasnier et al. « Narrow River Extraction from SAR Images
   Using Exogenous Information submitted to IEEE JSTARS »

   Copyright (C) 2020 Centre National d’Etudes Spatiales, CS GROUP, Télécom Paris

   This software is released under open source license GPL v.3 and is distributed WITHOUT ANY WARRANTY, read LICENSE.txt for further details.

"""

import numpy as np


from skimage.graph import route_through_array

from line_detector import *
from CRF_segmentation import *
import time
from utils import *


def ShortestPath(image_line_detector,coords_a_priori,n_reach , pow_line_detector, sar_image, pow_sar_image = 0, swot=True):
    '''

    :param image_line_detector: The response of the linear structures detector
    :param coords_a_priori: the list of reaches with coordinates
    :param n_reach:
    :param pow_line_detector:
    :param sar_image: the sar image
    :param pow_sar_image:
    :param swot: True if the image is from SWOT
    :type swot: bool
    :return:
    Compute the shortest path beetween the first and the last point whose coordinates are in x and y
    The output "label" is an image of the same shape as image_line_detector. label = 1 on the shortest path, 0 elsewhere.
    '''
    cap = np.squeeze(coords_a_priori[coords_a_priori[:,0] == n_reach,:])
    print(cap)
    image_line_detector = image_line_detector/np.amax(image_line_detector)
    sar_image_norm = sar_image/np.amax(sar_image)
    costs_image = (100*(1-image_line_detector))**pow_line_detector + (sar_image_norm * 100)**pow_sar_image
    if swot:
        costs_image = (100-100*sar_image_norm)**pow_sar_image + costs_image
    else:
        costs_image = (sar_image_norm)**pow_sar_image + costs_image
    indices, weight = route_through_array(costs_image,(int(cap[1]),int(cap[2])),(int(cap[3]),int(cap[4])))
    indices = np.array(indices).T
    labels  = np.zeros_like(image_line_detector)
    labels[indices[0], indices[1]] = 1
    return labels


def ShortestPathRiver(cost_map,coords_a_priori, list_reachs_in_river):
    '''
    :param cost_map: the previously computed cost array
    :param coords_a_priori: : the list of reaches with coordinates
    :param list_reachs_in_river: the list of the river numbers corresponding to all the reach number.
    Compute the shortest path beetween the first point of the first reach and the last point of the last reach
    The output "label" is an image of the same shape as image_line_detector. label = 1 on the shortest path, 0 elsewhere.
    '''

    coord_first_reach = np.squeeze(coords_a_priori[coords_a_priori[:,0] == min(list_reachs_in_river),:])
    coord_last_reach =  np.squeeze(coords_a_priori[coords_a_priori[:,0] == max(list_reachs_in_river),:])
    first = (int(coord_first_reach[1]),int(coord_first_reach[2]))
    last = (int(coord_last_reach[3]),int(coord_last_reach[4]))
    indices, weight = route_through_array(cost_map,first, last)
    indices = np.array(indices).T
    labels  = np.zeros_like(cost_map)
    labels[indices[0], indices[1]] = 1
    return labels



def SegementationMultipleRivers(image, coords_a_priori, **kwargs):
    '''

    :param image: 2darray
                the input SAR image
    :param coords_a_priori: n*5 int array
                            For each river, a list of its label and the coordinates of first and last point.
    :param kwargs:
    :return: lines_detector_response, centerlines, segmentation_rivers
    '''
    import matplotlib.pyplot as plt

    #plt.figure()
    #plt.imshow(image, vmin=0, vmax=display_threshold(image), cmap='gray')
    #add_nodes_display(coords_a_priori)
    #plt.show()
    #stopici
    image = image.astype(np.float)
    if 'reach_to_river' in kwargs:
        reach_to_river = kwargs['reach_to_river']
    else:
        reach_to_river = np.array([[i,i] for i in coords_a_priori[:,0]])
    list_num_rivers = sorted(set(list(reach_to_river[:,1])))
    print(list_num_rivers)
    t0 = time.time()
    lines_detector_response = compute_lines_detection(image, **kwargs)
    centerlines = np.zeros(image.shape, dtype=int) # All the centerlines
    centerlines_rivers = np.zeros(image.shape, dtype=int) # All the river centerlines
    segmentation_rivers = np.zeros(image.shape, dtype=int) # All the rivers
    t1 = time.time()
    for n_river in list_num_rivers:
        cost_map_river = np.full_like(centerlines, 100000)
        list_reachs_in_river = reach_to_river[reach_to_river[:,1]==n_river,0]
        for n_reach in list_reachs_in_river:
            print("Processing river {} reach {}, total {}. ".format(n_river, n_reach, coords_a_priori.shape[0], ))
            centerline_reach = ShortestPath(lines_detector_response, coords_a_priori,n_reach,kwargs['pow_line_detector'], image , kwargs['swot'])
            centerlines = centerlines + centerline_reach
            cost_map_river[centerline_reach > 0] = 1
        centerline_river = ShortestPathRiver(cost_map_river, coords_a_priori, list_reachs_in_river)
        centerlines_rivers = centerlines_rivers + centerline_river
        segmentation_river = CRF_segmentationV2(image, centerline_river, **kwargs)
        segmentation_rivers += (segmentation_river * n_river).astype(int)
    t2 = time.time()
    print("Image size: {:d} x {:d}, computing time: {:.2f}s including {:.2f} for line detection ".format(image.shape[0], image.shape[1], t2-t0, t1-t0))
    return (lines_detector_response, centerlines_rivers, segmentation_rivers, centerlines)
