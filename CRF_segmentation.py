# coding: utf8
"""
.. module:: Narrow Rivers Extraction 
    :synopsis: Runs the segmentation from the SAR image and centerline.

.. moduleauthor:: Nicolas Gasnier - CS GROUP - CNES - Télécom Paris

..
   This file is part of the river detection demonstrator for N.Gasnier et al. « Narrow River Extraction from SAR Images
 Using Exogenous Information submitted to IEEE JSTARS »
   Copyright (C) 2020 Centre National d’Etudes Spatiales, CS GROUP, Télécom Paris
   This software is released under open source license GPL v.3 and is distributed WITHOUT ANY WARRANTY, read LICENSE.txt for further details.

"""



import numpy as np
import matplotlib.pyplot as plt
import maxflow
import scipy.special
from Roewa import roewa
from scipy.special import gamma
from scipy.special import polygamma
from numpy import log
from utils import *

from scipy.ndimage import binary_dilation

def computeL1(I, R, L):
    return (L*I/R)+(1-L)*log(I) +L*np.log(R)+np.log(gamma(L))-L*np.log(L)



def CRF_segmentationV2(sar_image, centerline, beta=100, alpha=2.5, h=0.1, swot=True, L=4, verbose=False,eight_connex=True, select_river = True,  sigma_LoG = 3, eta = 15, **kwargs):
    """
    Region growing around the centerline  by graphcut.
    :param numpy.ndarray sar_image: The SAR image
    :param numpy.ndarray centerline: the initial centerline centerline
    :param float beta: linear regularization parameter
    :param float h: exponential regularization parameter
    :param float alpha: parameter for ROEWA computation
    :param bool swot: True if the image is from SWOT sensor
    :param int L: number of looks of the image
    :param bool verbose: True to display more intermediary results
    :return: boolean array water segmentation
    :rtype: numpy.ndarray
    """
    ## Graph cut binaire

    sar_image = sar_image + 0.0001

    bias = - np.log(L) + scipy.special.digamma(L)

    alpha = 1
    if swot:
        RiverEst = np.mean(
            np.log(
                sar_image[np.logical_and(centerline == 1, sar_image > np.percentile(sar_image[centerline == 1], 40))])) - bias
    else:
        RiverEst = np.mean(
            np.log(              
                sar_image[np.logical_and(centerline == 1, sar_image < np.percentile(sar_image[centerline == 1], 99))])) - bias
#    For non-SWOT images, the brightest pixels are excluded from the computation

    if verbose:
        plt.figure()
        plt.hist(sar_image[centerline == 1].flatten(), 100)
        plt.title("Hist centerline")
        plt.show(block=False)


    R = np.exp(RiverEst)
    print('Estimated reflectivity',R)

    L_0 = L+(-1+L)*np.log(L/R)-(-1+L)*(polygamma(0,L))+L*np.log(R)+np.log(gamma(L))-L*np.log(L)
    if verbose:
        print('L_1', L_0)

    TermesUnaires_Land = np.zeros_like(sar_image) + L_0
    TermesUnaires_Water = computeL1(sar_image, np.exp(RiverEst), L)
    edges_image = np.zeros_like(sar_image)
    edges_image[:,0] = 1
    edges_image[:,-1] = 1
    edges_image[1,:] = 1
    edges_image[-1,:] = 1
    TermesUnaires_Water = TermesUnaires_Water + edges_image * beta

    TermesUnaires_Land[centerline == 1] = 10 ** 10
    # Create the graph.
    g = maxflow.Graph[float]()  # Instanciation du graph
    # Add the nodes. nodeids has the identifiers of the nodes in the grid.
    nodeids = g.add_grid_nodes(
        sar_image.shape)  # Création d'une grille comportant un noeud non terminal pour chaque pixel de l'image
    # Add non-terminal edges with the same capacity.

    U_structure = np.array([[0, 1, 0],
                            [0, 0, 0],
                            [0, 0, 0]])
    D_structure = np.array([[0, 0, 0],
                            [0, 0, 0],
                            [0, 1, 0]])
    R_structure = np.array([[0, 0, 0],
                            [0, 0, 1],
                            [0, 0, 0]])
    L_structure = np.array([[0, 0, 0],
                            [1, 0, 0],
                            [0, 0, 0]])

    UR_structure = np.array([[0, 0, 1],
                            [0, 0, 0],
                            [0, 0, 0]])
    DL_structure = np.array([[0, 0, 0],
                            [0, 0, 0],
                            [1, 0, 0]])
    DR_structure = np.array([[0, 0, 0],
                            [0, 0, 0],
                            [0, 0, 1]])
    UL_structure = np.array([[1, 0, 0],
                            [0, 0, 0],
                            [0, 0, 0]])


    gradx, grady = roewa(sar_image, alpha)
    gradx45 = (gradx + grady)/np.sqrt(2) # pi/4 rotation of the gradient
    grady45 = (-gradx + grady)/np.sqrt(2) # pi/4 rotation of the gradient

    sensor_sign = {True:1,False:-1}
    S = sensor_sign[swot]


    
    U_Tr_U = beta * np.minimum(np.ones_like(grady), np.exp(S*(grady / h)))

    U_Tr_D = beta * np.minimum(np.ones_like(grady),np.exp(-S*(np.roll(grady,-1,1) / h)))
   
    U_Tr_R = beta * np.minimum(np.ones_like(gradx), np.exp(S*(gradx / h)))

    U_Tr_L = beta * np.minimum(np.ones_like(gradx), np.exp(-S*(np.roll(gradx,-1,0) / h)))

    h = h * np.sqrt(2) # for diagonal neighboorhood

    U_Tr_UR = beta * np.minimum(np.ones_like(grady), np.exp(S * (gradx45 / h)))

    U_Tr_DL = beta * np.minimum(np.ones_like(grady), np.exp(-S * (np.roll(gradx45, -1, 1) / h)))

    U_Tr_UL = beta * np.minimum(np.ones_like(gradx), np.exp(S * (grady45 / h)))

    U_Tr_DR = beta * np.minimum(np.ones_like(gradx), np.exp(-S * (np.roll(grady45, -1, 0) / h)))


    if verbose:
        h,w = sar_image.shape

        X,Y = np.meshgrid(np.arange(w), np.arange(h))
        n = 3
        plt.figure(figsize=(7, 7))
        plt.imshow(sar_image)
        plt.quiver(X[::n,::n],Y[::n,::n],gradx[::n,::n], -grady[::n,::n], np.log(np.sqrt(gradx[::n,::n]**2+grady[::n,::n]**2)),units="xy", scale=0.1, width= 0.5,headwidth=2, headlength=3, cmap="Reds")

        plt.show(block=False)
   
        plt.figure()
        plt.imshow(sar_image)
        plt.quiver(gradx, -grady)
        plt.show(block=False)




    if verbose:
        plt.figure()
        plt.imshow(gradx)
        plt.title("Gradient ROEWA horizontal")
        plt.show(block=False)

        plt.figure()
        plt.imshow(grady)
        plt.title("Gradient ROEWA vertical")
        plt.show(block=False)




        plt.figure()
        plt.imshow(U_Tr_U)
        plt.title("U_Tr_U")
        plt.show(block=False)

        plt.figure()
        plt.imshow(U_Tr_D)
        plt.title("U_Tr_D")
        plt.show(block=False)

    from scipy.ndimage import gaussian_laplace


    gLap = gaussian_laplace(np.log(sar_image),sigma_LoG)
    U_Tr_Lap =  S * eta * gLap



    if verbose:
        plt.figure()
        plt.imshow(U_Tr_Lap)
        plt.title('U_Tr_Lap')
        plt.show(block=False)


    g.add_grid_edges(nodeids, weights=U_Tr_U, structure = U_structure, symmetric=False)  # Add the terminal edges.
    g.add_grid_edges(nodeids, weights=U_Tr_U, structure = U_structure, symmetric=False)  # Add the terminal edges.
    g.add_grid_edges(nodeids, weights=U_Tr_D, structure = D_structure, symmetric=False)  # Add the terminal edges.
    g.add_grid_edges(nodeids, weights=U_Tr_L, structure = L_structure, symmetric=False)  # Add the terminal edges.
    g.add_grid_edges(nodeids, weights=U_Tr_R, structure = R_structure, symmetric=False)  # Add the terminal edges.
    if eight_connex:
        g.add_grid_edges(nodeids, weights=U_Tr_UR, structure=UR_structure, symmetric=False)  # Add the terminal edges.
        g.add_grid_edges(nodeids, weights=U_Tr_DL, structure=DL_structure, symmetric=False)  # Add the terminal edges.
        g.add_grid_edges(nodeids, weights=U_Tr_UL, structure=UL_structure, symmetric=False)  # Add the terminal edges.
        g.add_grid_edges(nodeids, weights=U_Tr_DR, structure=DR_structure, symmetric=False)  # Add the terminal edges.

    g.add_grid_tedges(nodeids, TermesUnaires_Land, TermesUnaires_Water + U_Tr_Lap)  # Ajout d'une arête de poids (im_obs[x,y]-m1)**2 entre chaque
    #  noeud non terminal, correspondant au pixel (x,y), et le noeud terminal correspondant à la source.
    # Ajout d'une arête de poids (im_obs[x,y]-m2)**2 entre chaque noeud non terminal correspondant au pixel (x,y), et le drain.

    # Find the maximum flow.
    flow = g.maxflow()
    if verbose:
        print("Max Flow:", str(flow))
    # Get the segments of the nodes in the grid.
    sgm = g.get_grid_segments(nodeids)  # Renvoie 1 si le pixel est du coté du drain après calcul de la coupe min, 0 si il est du coté de la source
    im_bin = np.int_(np.logical_not(sgm))

    if verbose:
        print('Median L0 on detected water',np.median(TermesUnaires_Water[im_bin==1]))
        print('Median L0 on detected land',np.median(TermesUnaires_Water[im_bin==0]))
        print('ratio',(np.sum(im_bin==0)/np.sum(im_bin==1)))

    if select_river:
        im_bin = binary_dilation(centerline,np.ones((3,3)),1000,im_bin)

    image_sar = sar_image
 
    contour_water = binary_dilation(im_bin) ^ im_bin
    gradx = gradx * contour_water
    grady = grady * contour_water

    if verbose:
        plt.figure()
        h,w = image_sar.shape
        X,Y = np.meshgrid(np.arange(w), np.arange(h))
        n = 1
        plt.imshow(color_display(sar_image, binary_dilation(im_bin) ^ im_bin, 1))
        plt.quiver(X[::n,::n],Y[::n,::n],gradx[::n,::n], grady[::n,::n], (np.sqrt(gradx[::n,::n]**2+grady[::n,::n]**2)),units="xy", scale=0.15, width= 0.5,headwidth=2, headlength=3, cmap="cool",minlength=0.00001)
        plt.show(block=False)

        gradx, grady = roewa(sar_image, alpha)


        plt.figure()
        h,w = image_sar.shape
        X,Y = np.meshgrid(np.arange(w), np.arange(h))
        n = 5
        plt.imshow(color_display(np.zeros_like(sar_image), binary_dilation(im_bin) ^ im_bin, 1))
        plt.quiver(X[::n,::n],Y[::n,::n],gradx[::n,::n], grady[::n,::n], (np.sqrt(gradx[::n,::n]**2+grady[::n,::n]**2)),units="xy", scale=0.15, width= 0.5,headwidth=2, headlength=3, cmap="cool",minlength=0.00001)
        gradx = gradx * contour_water

        grady = grady * contour_water
        n = 3
        plt.imshow(color_display(np.zeros_like(sar_image), binary_dilation(im_bin) ^ im_bin, 1))
        plt.quiver(X[::n, ::n], Y[::n, ::n], gradx[::n, ::n], grady[::n, ::n],
                   (np.sqrt(gradx[::n, ::n] ** 2 + grady[::n, ::n] ** 2)), units="xy", scale=0.15, width=0.5, headwidth=2,
                   headlength=3, cmap="cool", minlength=0.00001)

        plt.show(block=False)



    if verbose:
        plt.figure()
        plt.imshow(color_display(gradx, binary_dilation(im_bin) ^ im_bin, 1))
        plt.title('Water Segmentation Contour on x grad')
        plt.show(block=False)

        plt.figure()
        plt.imshow(color_display(grady, binary_dilation(im_bin) ^ im_bin, 1))
        plt.title('Water Segmentation Contour on x grad')
        plt.show(block=False)

        plt.figure()
        plt.imshow(color_display(np.maximum(np.abs(grady),np.abs(gradx)), binary_dilation(im_bin) ^ im_bin, 1))
        plt.title('Water Segmentation Contour on grad')
        plt.show(block=False)

        plt.figure()
        plt.imshow(color_display(U_Tr_D, binary_dilation(im_bin) ^ im_bin, 1))
        plt.title('Water Segmentation Contour on UtrD')
        plt.show(block=False)

        plt.figure()
        plt.imshow(color_display(U_Tr_U, binary_dilation(im_bin) ^ im_bin, 1))
        plt.title('Water Segmentation Contour on UtrU')
        plt.show(block=False)
    return (im_bin)
