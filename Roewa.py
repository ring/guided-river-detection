# coding: utf8
"""
.. module:: Narrow Rivers Extraction
    :synopsis: Computes the ROEWA gradient

.. moduleauthor:: Nicolas Gasnier - CS GROUP - CNES - Télécom Paris

..
   This file is part of the river detection demonstrator for N.Gasnier et al. « Narrow River Extraction from SAR Images
 Using Exogenous Information submitted to IEEE JSTARS »
   Copyright (C) 2020 Centre National d’Etudes Spatiales, CS GROUP, Télécom Paris
   This software is released under open source license GPL v.3 and is distributed WITHOUT ANY WARRANTY, read LICENSE.txt for further details.

"""

import numpy as np

def roewa(image, alpha):
    '''
    :param image: the SAR image
    :param alpha: the alpha parameter of the gradient computation
    :return gradx: the gradient in the x direction
    :return grady: the gradient in the y direction

    Computes the gradient of the SAR image according to the method described in "F. Dellinger, J. Delon, Y. Gousseau, J. Michel, and F. Tupin, “SARSIFT
    : A SIFT-Like Algorithm for SAR Images,” IEEE Transactions on Geoscience and Remote Sensing, vol. 53, no. 1, pp. 453–466, Jan 2015."
    '''
    I1 = np.copy(image)
    I2 = np.copy(image)
    I2[I1 < 1] = 1

    w = np.ceil(int(np.log(10) * alpha))
    w = int(w)
    list_coeffs = [np.exp((-np.abs(k) + 1) / alpha) for k in range(1, w + 2)]

    gradx1 = np.zeros_like(image).astype(float)
    grady1 = np.zeros_like(image).astype(float)
    gradh = np.zeros_like(image).astype(float)
    gradb = np.zeros_like(image).astype(float)
    gradg = np.zeros_like(image).astype(float)
    gradd = np.zeros_like(image).astype(float)

    for k in range(-w, w + 1):
        coeff = list_coeffs[np.abs(k)]
        gradx1[:,:] = gradx1[:,:] + np.roll(I1,k,axis=0) * coeff
        grady1[:,:] = grady1[:,:] + np.roll(I1,k,axis=1) * coeff

    for k in range(1, w + 1):
        coeff = list_coeffs[np.abs(k)]
        gradh[:,:] = gradh[:,:] + np.roll(grady1, k,axis=0) * coeff
        gradb[:,:] = gradb[:,:] + np.roll(grady1,-k,axis=0) * coeff
        gradg[:,:] = gradg[:,:] + np.roll(gradx1, k,axis=1) * coeff
        gradd[:,:] = gradd[:,:] + np.roll(gradx1,-k,axis=1) * coeff
    gradx = np.log(gradd / gradg)
    grady = np.log(gradh / gradb)
    return(gradx,grady)




